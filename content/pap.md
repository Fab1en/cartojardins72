---
author: "Yannick DUTHE"
date: 2014-09-28
title: Jardin Petit à petit !
tags: ["classique"]
bigimg: [{src: "/cartojardins72/img/pap.jpeg", desc: "Petit à petit"}]
---

Voici un beau jardin partagé à Mézière sous lavardin !

[Voir le PAD](https://paper.dropbox.com/doc/Jardin-collectif-Petit-a-petit--Ado2tYo80r_6gap0BB0I6w6uAg-PHSvZqWqBLqhq6K0XJlcG)

<iframe frameborder=0 height=100% width=100% src="https://hackmd.lescommuns.org/s/HyaHExaFN#"></iframe>
