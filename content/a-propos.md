---
title: À propos
subtitle: Les Jardins partagés au Mans
date: 2018-04-30T10:05:49+10:00
bigimg: [{src: "/cartojardins72/img/jardins_partages.jpeg", desc: "Jardins partagés"}]
---

Les jardins partagés sont tout simplement des lieux accessibles soit librement
soit moyennant une petite participation pour pouvoir particulièrement y faire
pousser des légumes
ou des fruits.
