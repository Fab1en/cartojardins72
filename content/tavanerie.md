---
author: "Yannick DUTHE"
date: 2014-09-28
title: Fête du jardin de la Tavanerie !
tags: ["nouveauté"]
bigimg: [{src: "/cartojardins72/img/tavanerie.jpeg", desc: "Tavanerie"}]
---

Voici un beau jardin partagé qui ouvre ses portes au Mans !
[test](https://paper.dropbox.com/doc/Jardin-partage-de-Tavano--AdowqOzhdG0yrM8CrraJZmCLAg-9BVGkHdl5z7WqgOMCL9iT)

<iframe src="https://hackmd.lescommuns.org/s/HyaHExaFN#"></iframe>
